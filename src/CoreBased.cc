#include "CoreBased.hh"
#include <iostream>
using namespace minibones;

CoreBased::CoreBased(ToolConfig& ptool_configuration, ostream& poutput, 
   Var pmax_id, const CNF& pclauses)
: tool_configuration(ptool_configuration)
, output(poutput)
, max_id(pmax_id)
, clauses(pclauses)
, whiten_clauses(pclauses)
, non_whiten_clauses(pclauses)
, solver_calls(0)
, solver_time(0)
, lifter(clauses)
, rotatable_computer(clauses)
{ /* */ }

CoreBased::~CoreBased() { /* */ }

void CoreBased::delete_bl_from_essential(unordered_set<int> & essential_lits, unordered_set<int> & new_whiten_cls){
  //* knock off backbone in essential
  //* add not essential to block
  //* clauses are not covered by backbone or essential are whiten 
  //* knock off clauses has essential
  for(size_t i = 0; i < clauses.size(); ++i){
  new_whiten_cls.insert(i);
    for(auto l : clauses[i]){
      int x = 0;
      if (!sign(l)) {
        x = var(l);
      } else{
        x = -var(l);
      }
      if(essential_lits.find(x) != essential_lits.end()){
        new_whiten_cls.erase(i);
      }
      else if(backbones.find(x) != backbones.end()){
        new_whiten_cls.erase(i);
      }
    }
  }
}

void CoreBased::get_blocking(vec<Lit> & new_cls, unordered_set<int> & essential_lits, string m, unordered_set<int> & new_whiten_cls){
  //* add new essential to block
  string add_assu = "";
  for(auto e : essential_lits){
    if(backbones.find(e) != backbones.end()) {
      continue;
    }
    if (e>0){
      new_cls.push(mkLit(abs(e), true));
      add_assu += to_string(abs(e)) + ",";
    } else{
      new_cls.push(mkLit(abs(e), false));
      add_assu += to_string(-abs(e)) + ",";
    }
  }
  if(new_cls.size() == 0){
    for(auto b : backbones){
      if(b>0){
        new_cls.push(mkLit(b, true));
        add_assu += to_string(b) + ",";
      } else{
        new_cls.push(mkLit(-b, false));
        add_assu += to_string(-b) + ",";
      }
    }
    //* clear new whiten cls, it should be empty
    new_whiten_cls.clear();
    //* add current model m to blocking
    for(size_t i = 0; i < m.size(); ++i){
      if(m.at(i) == '1'){
        new_cls.push(mkLit(i+1, true));
        add_assu += to_string(i+1) + ",";
      } else{
        new_cls.push(mkLit(i+1, false));
        add_assu += to_string(-(i+1)) + ",";
      }
    }
  }

  // dealing with the uncovered clause
  // cout<<"blocking clause before : "<<add_assu<<endl;
  if (new_whiten_cls.size() > 0){
    // number of new whiten clauses
    // cout<<"new whiten cls size : "<<new_whiten_cls.size()<<endl;
    // cout<<"new whiten clauses are :"<<endl;
    // for(auto i : new_whiten_cls){
    //   cout<<clauses[i]<<", ";
    // }
    // cout<<endl;

    LitBitSet new_whiten_lits;
    map<Lit, int> new_whiten_lits_cnt;
    map<int, vector<Lit>> ordered_lits_num;
    unordered_set<int> uncovered_cls;
    uncovered_cls.clear();
    for(auto i : new_whiten_cls){
      uncovered_cls.insert(i);
    }
    new_whiten_lits_cnt.clear();
    // get all new whiten lits (new_whiten_lits), and the number of appearence for each lits (new_whiten_lits_cnt, (Lit, 2))
    for(auto i : new_whiten_cls){
      for (auto l : clauses[i]){
        if (!new_whiten_lits.get(l)){
          new_whiten_lits.add(l);
          if(new_whiten_lits_cnt.find(l) == new_whiten_lits_cnt.end()){
            new_whiten_lits_cnt.insert(make_pair(l, 1));
          } else{
            new_whiten_lits_cnt.find(l)->second++; 
          }
        }
      }
    }
    // cout<<"new whiten lits  : "<<endl;
    // auto l = new_whiten_lits.infinite_iterator();
    // for(size_t i = 0; i < new_whiten_lits.size(); ++i){
    //   cout<<*++l<<" ";
    // }
    // cout<<endl;
    // cout<<"new whiten cls lits cnt : "<<endl;
    // ordered_lits_num, find all literals that appears k times in the clause (2, {a,b,c})
    for(auto l : new_whiten_lits_cnt){
      // cout<<"literal "<<l.first<<" appears : "<<l.second<<",";
      if (ordered_lits_num.find(l.second) != ordered_lits_num.end()){
        ordered_lits_num.find(l.second)->second.push_back(l.first);
      } else {
        vector<Lit> num_lits;
        num_lits.clear();
        num_lits.push_back(l.first);
        ordered_lits_num.insert(make_pair(l.second, num_lits));
      }
    }
    // cout<<endl;

    // cout<<"ordered lits appear num"<<endl;
    for(size_t i = new_whiten_cls.size() - 1; i > 0; --i){
      if (ordered_lits_num.find(i) == ordered_lits_num.end()){
        continue;
      }
      // cout<<"lits with appear number "<<i<<" are : "<<ordered_lits_num.find(i)->second<<endl;
      for(auto l : ordered_lits_num.find(i)->second){
        if ((m.at(var(l) - 1) == '1' && !sign(l)) || (m.at(var(l)-1) == '0' && sign(l)) ){
          // cout<<m.at(var(l) - 1)<<endl;
          new_cls.push(~l);
          // cout<<"add lit "<<~l<<" to blcoking clause"<<endl;
          if(sign(l)){
            add_assu += to_string(-var(l)) + ",";
            for(auto c : neg_lit_in_clauses[var(l)]){
              if(uncovered_cls.find(c) != uncovered_cls.end()) {
                uncovered_cls.erase(c);
              }
            }
          } else {
            add_assu += to_string(var(l)) + ",";
            for(auto c : pos_lit_in_clauses[var(l)]){
              if(uncovered_cls.find(c) != uncovered_cls.end()) {
                uncovered_cls.erase(c);
              }
            }
          }
        }
        // cout<<"uncovered_cls size : "<<uncovered_cls.size()<<endl;
        if(uncovered_cls.size() == 0){
          break;
        }
      }
    }
  }


  num_blocking_lits += new_cls.size();
  // cout<<"add blocking clause : "<<new_cls<<endl;
}

void CoreBased::allsat(){
  //TODO if backbone covered the formula, terminate
  //TODO use assumptions for sat computing
  unordered_set<string> new_models;
  new_models.clear();
  all_models.clear();
  num_blocking_lits = 0;
  // blocking all clauses from the backbone models
  int cnt = 1;
  sat_solver.cancelUntil(0);
  for(auto m : models){
    // cout<<"executing "<<cnt<<" model"<<endl;
    unordered_set<int> essential_lits = essential.find(m)->second;
    unordered_set<int> new_whiten_cls;
    new_whiten_cls.clear();
    vec<Lit> new_cls;
    new_cls.clear();
    delete_bl_from_essential(essential_lits, new_whiten_cls);
    get_blocking(new_cls, essential_lits, m, new_whiten_cls);
    sat_solver.addClause(new_cls);
    // cout<<"adding blocking cls "<<new_cls<<" to sovler"<<endl;
    cnt++;
  }

  // find a new model with all the blocking
  vec<Lit> assu;
  assu.clear();
  for(auto b : backbones){
    if(b>0){
      assu.push(mkLit(b, false));
    } else {
      assu.push(mkLit(-b, true));
    }
  }
  bool ret = run_sat_solver(assu);
  while(ret){
    vec<Lit> new_model;
    new_model.clear();
    new_model.push(mkLit(1,true));
    for (Var variable = 1; variable < sat_solver.model.size(); ++variable) {
        const lbool value = sat_solver.model[variable];
        if (value != l_Undef) {
          const Lit l = mkLit(variable, value==l_True);
          new_model.push(l);
        }
    }
    whiten(new_model, false);
    string new_m = string_model(sat_solver.model);
    unordered_set<int> essential_lits = essential.find(new_m)->second;
    unordered_set<int> new_whiten_cls;
    new_whiten_cls.clear();
    vec<Lit> new_cls;
    new_cls.clear();
    delete_bl_from_essential(essential_lits, new_whiten_cls);
    get_blocking(new_cls, essential_lits, new_m, new_whiten_cls);
    sat_solver.addClause(new_cls);
    if(all_models.find(new_m) == all_models.end()){
      new_models.insert(new_m);
      all_models.insert(new_m);
      if(all_models.size() % 1000 == 0 ){
        cout<<"model number : "<<all_models.size()<<endl;
      }
    }
    ret = run_sat_solver(assu);
  }

}

void get_essential_assu(string constraint, vec<Lit> & essential_assu){
  int end = constraint.size();
  while(constraint.find(",") != string::npos){
    int pos = constraint.find(",");
    string str_l = constraint.substr(0, pos);
    constraint = constraint.substr(pos + 1, end - 1);
  }

}

string CoreBased::string_model(vec<lbool> &model){
  string str = "";
  for(int i = 1; i <= max_id; ++i){
    if(model[i] == l_True){
      str += "1";
    } else {
      str += "0";
    }
  }
  return str;
}

string CoreBased::string_model(vec<Lit> &model){
  string str = "";
  for(int i = 1; i <= max_id; ++i){
    if(sign(model[i])){
      str += "1";
    } else {
      str += "0";
    }
  }
  return str;
}

/**
Compute Whiten clause with a given model
*/
void CoreBased::whiten(vec<Lit>& model, bool first){
  unordered_set<int> essential_lits;
  essential_lits.clear();
  unordered_set<int> whiten_cls_id;
  whiten_cls_id.clear();
  unique_satisfy_literals.clear();
  whiten_clauses.clear();
  non_whiten_clauses.clear();
  int satisfied_literal_cnt = 0;
  int i = 0;
  FOR_EACH(ci,clauses) {
    satisfied_literal_cnt = 0;
    Lit unique_satisfy;
    FOR_EACH(li, *ci){
      if(sign(*li) != sign(model[var(*li)])){
        satisfied_literal_cnt++;
        unique_satisfy = *li;
      }
      if (first) {
        if(sign(*li) == false){
          pos_lit_in_clauses[var(*li)].push_back(i);
        } else if(sign(*li) == true){
          neg_lit_in_clauses[var(*li)].push_back(i);
        }
      }
    }
    if(satisfied_literal_cnt > 1){
      whiten_clauses.push_back(*ci);
      whiten_cls_id.insert(i);
    } else {
      if (sign(unique_satisfy)){
        essential_lits.insert(-var(unique_satisfy));
      } else {
        essential_lits.insert(var(unique_satisfy));
      }
      non_whiten_clauses.push_back(*ci);
      unique_satisfy_literals.add(~unique_satisfy);
    }
    i++;
  }

  essential.insert(make_pair(string_model(model), essential_lits));
  whiten_cls.insert(make_pair(string_model(model), whiten_cls_id));

}

void CoreBased::flip1(){
  CNF new_whiten_clauses;
  CNF new_non_whiten_clauses;
  FOR_EACH(ci,non_whiten_clauses){
    bool white = false;
    FOR_EACH(li,*ci){
      if(!unique_satisfy_literals.get(~*li) && !unique_satisfy_literals.get(*li)){
        new_whiten_clauses.push_back(*ci);
        whiten_clauses.push_back(*ci);
        white = true;
        break;
      } 
    }
    if(white == false){
      new_non_whiten_clauses.push_back(*ci);
    }
  }
  unique_satisfy_literals.clear();
  LitBitSet new_non_whiten_literals;
  FOR_EACH(ci, new_non_whiten_clauses){
    FOR_EACH(li, *ci){
      if(sign(*li) != sign(modelLit[var(*li)])){
        new_non_whiten_literals.add(~*li);
        unique_satisfy_literals.add(~*li);
        break;
      } 
    }
  }
}

/**
	compute whiten variables (approximation) according to the current non backbone
	triggered when there is no more rotatble literals
	until no update
	return the complement of whiten variaables (approximations)
*/
void CoreBased::extend(vec<Lit> & high_possibile_backbone){
	LitBitSet non_backbone_set;
	for(int i = 0; i < non_backbone.size(); ++i){
		non_backbone_set.add(non_backbone[i]);
		non_backbone_set.add(~non_backbone[i]);
	}
	CNF new_whiten_clauses;
	CNF new_non_white_clauses;
	FOR_EACH(ci,clauses){
		bool whiten = false;
		FOR_EACH(li,*ci){
			if(non_backbone_set.get(*li)){
				new_whiten_clauses.push_back(*ci);
				whiten = true;
				break;
			}
		}
		if(!whiten){
			new_non_white_clauses.push_back(*ci);
		}
	}
	LitBitSet new_non_white_literals;
	FOR_EACH(ci, new_non_white_clauses){
		FOR_EACH(li,*ci){
			if(sat_solver.model[var(*li)] == l_True){
				new_non_white_literals.add(~*li);
			}
		}
	}
}

void CoreBased::compute_white_literals(vec<lbool> &model, vec<Lit> &white_literlas){
  LitBitSet white_clauses_of_one_model;
  for(int i = 0; i < white_literlas.size(); ++i){
    Lit l = white_literlas[i];
    if(sign(l)){
      for(size_t j = 0; j < pos_lit_in_clauses[var(l)].size(); ++j){
        int cls_id = pos_lit_in_clauses[var(l)][j];
        if(clauses[cls_id].size() == 2 ) continue;
        white_clauses_of_one_model.add(mkLit(cls_id,true));
      }
    } else {
      for(size_t j = 0; j < neg_lit_in_clauses[var(l)].size(); ++j){
        int cls_id = neg_lit_in_clauses[var(l)][j];
        if(clauses[cls_id].size() == 2) continue;
        white_clauses_of_one_model.add(mkLit(cls_id,true));
      }
    }
  }
  LitBitSet not_non_backbone;
  int cls_id = 0;
  FOR_EACH(ci,clauses){
    if(white_clauses_of_one_model.get(mkLit(cls_id,true))){
      cls_id++;
      continue;
    } else {
      cls_id++;
      FOR_EACH(li,*ci){
        if((model[var(*li)] == l_True) != sign(*li)){
          not_non_backbone.add(*li);
        }
      }
    }
  }
  int count = 0;
  for(int i = 1; i < sat_solver.nVars(); ++i){
    if(!not_non_backbone.get(mkLit(i,true)) && !not_non_backbone.get(mkLit(i,false))){
      non_backbone.push(mkLit(i));
      if(might_be.remove(mkLit(i,true)) || might_be.remove(mkLit(i,false))){
        count++;
      }
    }
  }
  vec<Lit> test;
}

void CoreBased::get_max_coverage_ordered_literals (vector<int> &pos_lit, vector<int> &neg_lit) {
  // compute coverage of each literals in clauses
  int maximal_coverage = 0;
  pos_lit.push_back(0);
  neg_lit.push_back(0);
  for(int i = 1; i <= max_id; ++i){
    pos_lit.push_back(0);
    neg_lit.push_back(0);
  }
  FOR_EACH(ci, clauses) {
    FOR_EACH(li, *ci){
      const Lit l = *li;
      if(sign(l)){
        pos_lit[var(l)]++;
        if(pos_lit[var(l)] > maximal_coverage){
          maximal_coverage = pos_lit[var(l)];
        }
      } else {
        neg_lit[var(l)]++;
        if(neg_lit[var(l)] > maximal_coverage){
          maximal_coverage = neg_lit[var(l)];
        }
      }
    }
  }

  vector<int> tmp;
  tmp.push_back(0);
  coverage_number.push_back(tmp);
  for(int i = 1; i <= maximal_coverage; ++i){
    coverage_number.push_back(tmp);
  }
  for(int i = 1; i <= max_id; ++i){
    coverage_number[pos_lit_in_clauses[i].size()].push_back(i);
    coverage_number[neg_lit_in_clauses[i].size()].push_back(-i);
  }

  // // coverage_number [1] means the varibale that has the covergae of 1
  // for(size_t  i = 0; i < coverage_number.size(); ++i){
  //   cout<<i<<endl;
  //   vector<int> cov_int = coverage_number[i];
  //   for(size_t j = 0; j < cov_int.size(); ++j){
  //     cout<<cov_int[j]<<" ";
  //   }
  //   cout<<endl;
  // }

}

bool CoreBased::initialize() {
  //initialize the solver
  propagate_cnt = 0;
  cov_cnt = 0;
  test_backbones_2len_cls_cnt = 0;
  sort_make_chunk = 0;
  whiten_cnt = 0;
  test_high_possible = false;
  whiten_clauses.clear();
  non_whiten_clauses.clear();
  coverage_number.clear();
  sort_might_be.clear();
  sat_solver.new_variables(max_id);
  vec<Lit> ls;
  int i = 0;
  vector<int> empty;
  for(int i = 0; i < sat_solver.nVars(); ++i){
    pos_lit_in_clauses.push_back(empty);
    neg_lit_in_clauses.push_back(empty);
  } 
  FOR_EACH(ci, clauses) {
    ls.clear ();
    FOR_EACH(li, *ci) {
      const Lit l = *li;
      assert (var(l) <= max_id);
      ls.push(l);
      if(sign(*li) == false){
        pos_lit_in_clauses[var(*li)].push_back(i);
      } else if(sign(*li) == true){
        neg_lit_in_clauses[var(*li)].push_back(i);
      }
      // .cnf 34 0, l=(-34)
    }
    sat_solver.addClause(ls);
    ++i;
  }

  // get the first model
  vector<int> pos_lit;
  vector<int> neg_lit;
  initial_assu.clear();
  // cout<<"initial assumption : "<<initial_assu.size()<<endl;
  auto t1 = read_cpu_time();  
  bool result = run_sat_solver();
  // cout<<"initial assumption is "<<(result ? "sat" : "unsat")<<endl;
  cout<<"i ast : "<<read_cpu_time() - t1<<endl;
  previous_solver_time = read_cpu_time() - t1;
  // if (!result) {
  //   result = run_sat_solver();
  //   // cout<<"compute without initial assu time : "<<read_cpu_time() - t1<<endl;
  // }
  if (!result) {// the whole instance is not satisfiable
    return false;
  }

  const vec<lbool>& model = sat_solver.model;
  models.insert(string_model(sat_solver.model));
  sat_solver.model.copyTo(pre_model);
  // cout<<"model ";
  const Var max_model_var = std::min(model.size()-1,max_id);
  modelLit.push(mkLit(1,true));
  for (Var variable = 1; variable <= max_model_var; ++variable) {
      const lbool value = model[variable];
      if (value != l_Undef) {
        const Lit l = mkLit(variable, value==l_True);
        // cout<<(sign(l) ? "+" : "-")<<var(l)<<" ";
        modelLit.push(l);
      }
  }

  // initialize whiten algorithm using the given mode whiten(modelLit, false);
  // flip1();
  whiten(modelLit, false);
  get_max_coverage_ordered_literals(pos_lit, neg_lit);


  // change initialzie might_be to wv target
  for(int i = 0; i < modelLit.size(); ++i){
    // visited.add(~modelLit[i]);
    //if(!unique_satisfy_literals.get(modelLit[i])) continue;
    might_be.add(~modelLit[i]);
  }

  process_model(sat_solver.model);

  // if the coverage of a literal is greater than 10
  for(size_t m = 0 ; m < coverage_number.size(); ++m){
  // for(size_t m = coverage_number.size() - 1 ; m > 0; --m){      
    vector<int> lits = coverage_number[m];
    if (lits.size() == 0) continue;
    // we try to make this literals as wv
    vector<int> clses;   
    for(size_t j = 0; j < lits.size(); ++j){
      Lit l = ~modelLit[lits[j]];
      if(might_be.get(l)){
        if(! added_might_be.get(l)) {
         sort_might_be.push_back(l); 
         added_might_be.add(l);   
        }
      } 
    }
  }

  return true;
}


void CoreBased::run() {
  LitBitSet relaxed; // literals removed from assumptions
  LitBitSet first_uc;// store the first uc in every iteration
  vec<Lit>  assumptions; // assumptions passed onto the SAT solver
  vec<Lit>  old_assumptions; // assumptions in prev.\ iteration of inner loop
  bool get_decision = false;
  int chunk = 100;
  while (might_be.size()) {
    if(!get_decision){
      get_decision = true;
      for(auto i = 0; i < sat_solver.decisionLits.size(); ++i){
        old_assumptions.push(sat_solver.decisionLits[i]);
        decision_lits.push(sat_solver.decisionLits[i]);
        LitBitSet test_decision;
        test_decision.add(~sat_solver.decisionLits[i]);
        vector<Lit> implies;
        implies.clear();
        for(int j = 0; j < sat_solver.impliedLits[i].size(); ++j){
          implies.push_back(sat_solver.impliedLits[i][j]);
        }
        implied_lits.push_back(implies);
        test_backbones(test_decision, false);
      }
    }
    size_t chunk_size = make_chunk(old_assumptions, chunk);
    relaxed.clear();
    while (true) { 
      assumptions.clear();
      for (int i=0; i<old_assumptions.size(); ++i) {
        const Lit l = old_assumptions[i];
        if (relaxed.get(l)) continue;
        assumptions.push(l);
      }
      if(assumptions.size() == 0) break;
      const bool flipped = run_sat_solver(assumptions);
      if (flipped == true) { 
        difference.clear();
        process_model(sat_solver.model);
        for(int i = 1; i < sat_solver.nVars(); ++i){
          if(pre_model[i] != sat_solver.model[i]){
            might_be.remove(mkLit(i,true));
            might_be.remove(mkLit(i,false));
            difference.push(mkLit(i));
          }
        }
        compute_white_literals(sat_solver.model, difference);
        sat_solver.model.copyTo(pre_model);
        break; // we are done with the chunk
      } else if(flipped == false){ 
        const auto& conflict = sat_solver.conflict;
        if (sat_solver.conflict.size()==1) { // A unit conflict means backbone. 
          mark_backbone(conflict[0]); 
        }
        vec<Lit> learnt;
        for (int i=0; i<conflict.size(); ++i) {
          relaxed.add(~conflict[i]);
          learnt.push(conflict[i]);
        }
        assert(relaxed.size()<=chunk_size);
        if(conflict.size() > 1){
          first_uc.clear();
          first_uc.add(~conflict[0]);
          sat_solver.addClause(learnt);
          learnt.clear();
          test_backbones(first_uc, true);
          // cout<<"test first uc time "<<read_cpu_time()-t<<endl;
        }
      } 
      if (relaxed.size()>=chunk_size) {
        test_backbones(relaxed, true);
        break; // we are done with the chunk
        }
      old_assumptions.clear();
      assumptions.copyTo(old_assumptions);
    }
  }
  // cout<<"propagate number : "<<propagate_cnt<<endl;
  // cout<<"cov cnt : "<<cov_cnt<<endl;
  // cout<<"test backbones with 2len cls : "<<test_backbones_2len_cls_cnt<<endl;
  // cout<<"whiten cnt : "<<whiten_cnt<<endl;
}

void CoreBased::test_backbones(LitBitSet & negliterals, bool len2_cls){
  // testing backbone using the fact that if for every clauses of len 2, 
  // the rest of the neighbour can't be the same at the same time,
  // then the given lit is a backbone literal.
  previous_conflicts = sat_solver.conflicts;
  vec<Lit> assu;
  LitBitSet test;
  FOR_EACH(li, negliterals){
    Lit neg = *li;
    Lit l = ~neg;
    if (!might_be.get(l)) {
      negliterals.remove(neg);
      continue;
    }
    vector<int> clses;
    if(!(sign(neg))){
      clses = pos_lit_in_clauses[var(neg)];
    } else {
      clses = neg_lit_in_clauses[var(neg)];
    }
    for(size_t m = 0; m < clses.size(); ++m){
      LitSet cls = clauses[clses[m]];
      int free_cnt = 0;
      Lit free_lit;
      FOR_EACH(tl, cls){
        if(var(*tl) == var(neg)) continue;
        if (!must_be.get(~*tl)) {
          free_cnt++;
          free_lit = *tl;
        }
      }
      if(free_cnt == 1){
        assu.push(free_lit);
      }
    }
    if(assu.size() > tool_configuration.test_by_2len){
      test_backbones_2len_cls_cnt++;
      bool ret = run_sat_solver(assu);
      //bool ret = run_sat_solver();
      if(!ret) {
        mark_backbone(l);
        negliterals.remove(neg);
      } else {
        process_model(sat_solver.model);
        test.add(neg);
      }
    } else {
      test.add(neg);
    }
  }
  if(test.size() > 0) test_backbones(test);
}

void CoreBased::test_backbones(LitBitSet& negliterals) {
  previous_conflicts = sat_solver.conflicts;
  vec<Lit> assumptions(1);
  FOR_EACH (li, negliterals) {
    const Lit negl=*li;
    const Lit    l=~negl;
    if (!might_be.get(l)) {
      negliterals.remove(negl);
      continue;
    }
    assumptions[0]=negl;
    const bool flipped = run_sat_solver(assumptions);
    if (flipped == true) {
      difference.clear();
      process_model(sat_solver.model);
      for(int i = 0; i < sat_solver.nVars(); ++i){
        if(pre_model[i] != sat_solver.model[i]){
          difference.push(mkLit(i,true));
        }
      }
      negliterals.remove(negl);
      sat_solver.model.copyTo(pre_model);
 }
   else if(flipped == false) {
      mark_backbone(l);
      negliterals.remove(negl);
    } else {
    }
  }
}

void CoreBased::process_model(const vec<lbool>& model) {
  modelLit.clear();
  modelLit.push(mkLit(1,true));
  for (Var variable = 1; variable <= max_id; ++variable) {
      const lbool value = model[variable];
      if (value != l_Undef) {
        const Lit l = mkLit(variable, value==l_True);
        // cout<<(sign(l) ? "+" : "-")<<var(l)<<" ";
        modelLit.push(l);
      }
  }
  whiten(modelLit, false);
  for(int i = 0; i < modelLit.size(); ++i){
    if(!unique_satisfy_literals.get(modelLit[i])) {
      if (might_be.get(modelLit[i]) || might_be.get(~modelLit[i]))
        whiten_cnt++;      
      //might_be.remove(modelLit[i]);
      //might_be.remove(~modelLit[i]);
    }
  }
  
  

  if (tool_configuration.get_scdc_pruning()) {
    vec<lbool> rmodel;
    vec<Lit> assumptions;
    lifter.reduce(assumptions, model, rmodel);
    assert(lifter.is_model(rmodel));
    process_pruned_model(rmodel);
  } else if (tool_configuration.get_rotatable_pruning ()) {
    VarSet pruned;
    rotatable_computer.get_rotatables(model, pruned);
    FOR_EACH (vi, pruned) {
      const Lit pl = mkLit(*vi);
      const Lit nl = ~pl;
      debone(pl);
      debone(nl);
    }
    process_pruned_model(model);
  } else {
    process_pruned_model(model);
  }
}

void CoreBased::process_pruned_model(const vec<lbool>& model) {
  for (Var variable = 1; variable < model.size(); ++variable) {
    const lbool value = model[variable];
    const Lit   pos_lit = mkLit(variable);
    const Lit   neg_lit = ~mkLit(variable);
    if (value != l_True) might_be.remove(pos_lit);
    if (value != l_False) might_be.remove(neg_lit);
  }

  for (Var variable=std::max(model.size(),1); variable<=max_id; ++variable) {
    const Lit pos_lit = mkLit(variable);
    might_be.remove( pos_lit);
    might_be.remove(~pos_lit);
  }
}

size_t CoreBased::make_chunk(vec<Lit>& literals, int chunk) {
  // cout<<"chunk size"<<chunk<<endl;
  int might_be_size = might_be.size();
  // cout<<"might_be size"<<might_be_size<<endl;
  int real_chunk_size = (int)std::min(chunk, might_be_size); 
  // cout<<"real chunk size"<<real_chunk_size<<endl;
  literals.clear();
  int count = 0;
  FOR_EACH (li, might_be) {
    count++;
    if(count < 0) continue;
    if (literals.size() >= real_chunk_size) break;
    // if (sort_make_chunk > sort_might_be.size()) {
      const Lit lit = *li;
      literals.push(~lit);
    // } else {
    //   const Lit lit = sort_might_be[sort_make_chunk];
    //   sort_make_chunk++;
    //   literals.push(~lit);
    // }
  }
  return literals.size();
}

bool CoreBased::is_backbone(const Lit& lit) const 
{ return must_be.get(lit); }

void CoreBased::mark_backbone(Lit lit) {
  pure_mark_backbone(lit);

  for(int i = 0; i < decision_lits.size(); ++i){
    if(var(lit) == var(decision_lits[i])){
      for(int j = 0; j < implied_lits[i].size(); ++j){
        if(!is_backbone(implied_lits[i][j])){
          mark_backbone(implied_lits[i][j]);
        }
      }
    }
  }

  vector<int> clses;
  clses.clear();
  if (!sign(lit)){
    clses = pos_lit_in_clauses[var(lit)];
  } else {
    clses = neg_lit_in_clauses[var(lit)];
  }
  vector<int> neg_clses;
  neg_clses.clear();
  if (!sign(~lit)){
    neg_clses = pos_lit_in_clauses[var(lit)];
  } else {
    neg_clses = neg_lit_in_clauses[var(lit)];
  }


  // find cov1 literals
  if (clses.size() == 1){
    int cls_id = clses[0];
    LitSet cls = clauses[cls_id];
    FOR_EACH(l,cls){
      if(var(*l) != var(lit) && !must_be.get(~*l)) {
        mark_backbone(~*l);
        cov_cnt++;
      }
    }
  }

  // find cov2 literals
  else if (clses.size() == 2){
    clses.clear();
    if(!sign(lit)){
      clses = pos_lit_in_clauses[var(lit)];
    } else {
      clses = neg_lit_in_clauses[var(lit)];
    }
    LitSet cls1 = clauses[clses[0]];
    LitSet cls2 = clauses[clses[1]];
    vec<Lit> assu;    
    FOR_EACH(l, cls1){
      if (var(*l) == var(lit)) continue;
      if (must_be.get(*l)) {
        FOR_EACH(tl, cls2){
          if(var(*tl) == var(lit) ) continue;
          if (!must_be.get(~*tl)){
            mark_backbone(~*tl);
            cov_cnt++;
          }
        }
      }
    }
    FOR_EACH(l, cls2){
      if (var(*l) == var(lit)) continue;
      if (must_be.get(*l)) {
        FOR_EACH(tl, cls1){
          if(var(*tl) == var(lit)) continue;
          if (!must_be.get(~*tl)){
            mark_backbone(~*tl);
            cov_cnt++;
          }
        }
      }
    }
  }

  else if (clses.size() > 2){
    int no_backbone_cnt = 0;
    LitSet backbone_cls;    
    for(size_t m = 0; m < clses.size(); ++m){
      LitSet cls = clauses[clses[m]];
      bool has_backbone = false;
      FOR_EACH(tl, cls){
        if (var(*tl) == var(lit)) continue;
        if (must_be.get(*tl)) {
          has_backbone = true;
          continue;
        }
        if(might_be.get(*tl)){
        }
      }
      if (!has_backbone){
        no_backbone_cnt++;
        backbone_cls = cls;
      }
    }
    if( no_backbone_cnt == 1) {
      FOR_EACH(tl, backbone_cls){
        if(var(*tl) == var(lit)) continue;
        if (!must_be.get(~*tl)){
          mark_backbone(~*tl);
          cov_cnt++;
        }
      }
    }
  }

  // propagate
  Lit neg = ~lit;
  for (size_t m = 0; m < neg_clses.size(); ++m){
    LitSet cls = clauses[neg_clses[m]];
    int not_backbone_cnt = 0;
    Lit bl;
    FOR_EACH(l, cls){
      if(var(*l) == var(neg)) continue;
      if (!must_be.get(~*l)){
        not_backbone_cnt++;
        bl = *l;
      }
    }
    if (not_backbone_cnt == 1){
      if(!must_be.get(bl)) {
        mark_backbone(bl);
        propagate_cnt++;
      }
    }
  }
}


