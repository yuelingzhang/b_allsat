#include <vector>
#include <functional>
#include "MiniSatExt.hh"
#include "auxiliary.hh"
#include "Lifter.hh"
#include "ToolConfig.hh"
#include "Rotatable.hh"
#include "BackboneInformation.hh"
#include "LitBitSet.hh"
#include <unordered_set>
#include <unordered_map>
#include <map>
#include <string>
#include <string.h>
#include <stdio.h>
using namespace std;
using Minisat::MiniSatExt;
using Minisat::Var;
using Minisat::Lit;
using Minisat::vec;

namespace minibones {
    class CoreBased : public BackboneInformation {
    public: 
      CoreBased(ToolConfig& tool_configuration, 
                std::ostream& output, 
                Var max_id, const CNF& clauses);
      virtual ~CoreBased();
      bool initialize();// initialize, returns true iff the instance is SAT
      void init_allsat();
      void run();// start the worker, initialize must be called first.
      void allsat(); // start allsat computing
      virtual bool is_backbone(const Lit& literal) const;
      size_t  get_solver_calls() const { return solver_calls; }
      double  get_solver_time() const  { return solver_time; }
      size_t  num_blocking_lits;

      unordered_set<string> models;   // all models find during backbone computing
      unordered_set<int> backbones;   // all backbones find during backbone computing
      unordered_map<string, unordered_set<int>> essential;
      unordered_map<string, unordered_set<int>> whiten_cls;
      unordered_set<string> all_models;
      unordered_map<string, unordered_set<int>> add_partial_assu;

        private: 
      LitBitSet           might_be; 
      LitBitSet           must_be;  
      LitBitSet           visited;

      const ToolConfig&   tool_configuration;
      ostream&            output;
      const Var           max_id;
      const CNF&          clauses;
      CNF                 whiten_clauses;
      /** non whiten clauses with a given model */
      CNF                 non_whiten_clauses;
      vec<Lit>            modelLit;
      vec<lbool>          pre_model;
      bool                budget;
      /** accurate non-backbone, ordered by the layer of its generation */
      vec<Lit>            non_backbone;
      /** unqiue satisfiy literal with a given model, the satisfied literal in non whiten clause */
      LitBitSet           unique_satisfy_literals;
      LitBitSet           white_literals;
      /** outer vec stores lit, started from [1], inner lit stores clauses number, clauses[i] is stored as i, indicates the i+1 th clauses, since clauses is started from 1 */
      vector<vector<int>> pos_lit_in_clauses;
      vector<vector<int>> neg_lit_in_clauses;
      vec<Lit>            flip_literals;
      vec<Lit>            difference;
      bool                test_high_possible;

      size_t              solver_calls;// number of solver calls, for statistical purposes
      double              solver_time;// for statistical purposes
      double              previous_solver_time; // last SAT solving call time
      size_t              base_conflicts;// conflicts cnt of the first SAT testing
      size_t              previous_conflicts;// previous conflicts cnf of SAT testing
      size_t              base_time;//time to compute the first SAT testing
      float               previous_white_clauses_ratio = 0;

      double              conflict_ratio;// the ratio of availiable new conflicts


      Lifter              lifter;// used to reduce models via lifting
      Rotatable           rotatable_computer;// used to get rotatable variables

      int                 cov_cnt;
      int                 propagate_cnt;
      int                 test_backbones_2len_cls_cnt;
      vector<vector<int>> coverage_number;
      vector<Lit>         sort_might_be;
      LitBitSet           added_might_be;
      int                 sort_make_chunk;
      int                 whiten_cnt;
      vec<Lit>            initial_assu;

      vec<Lit>            decision_lits;
      vector<vector<Lit>> implied_lits;


      MiniSatExt sat_solver;
      void process_model(const vec<lbool>& model);// calls pruning techniques and debones based of the model
      void process_pruned_model(const vec<lbool>& model);// debones everything not in the model
      size_t make_chunk(vec<Lit>& literals, int chunk_size);
      inline bool debone(Lit literal);// mark a literal as not a backbone
      void mark_backbone(Lit literal);// mark a literal as a backbone
      inline void pure_mark_backbone(Lit literal);// mark a literal as a backbone      
      void test_backbones(LitBitSet& literals);
      inline bool run_sat_solver(const vec<Lit>& assumptions);
      inline lbool run_sat_solver_limited(const vec<Lit>& assumptions);
      inline bool run_sat_solver();

      void whiten(vec<Lit>& model, bool first);
      /** extend whiten literal by only flip one literal at each iteration*/
      void flip1();
      void compute_white_literals(vec<lbool> &model, vec<Lit> &lits);
      void extend(vec<Lit> & high_possible_backbone);
      void get_max_coverage_ordered_literals(vector<int> &pos_literals, vector<int> &neg_literals);               // return a vec of literals, with the order of max coverage
      void test_backbones(LitBitSet & negliterals, bool len2_cls);

      string string_model(vec<lbool> &model);
      string string_model(vec<Lit> &model);

      void delete_bl_from_essential(unordered_set<int> & essential_lits, unordered_set<int> & new_whiten_cls);
      void get_blocking(vec<Lit> & new_cls, unordered_set<int> & essential_lits, string m, unordered_set<int> & new_whiten_cls);

  };
    
  inline bool CoreBased::debone(Lit literal) { 
    assert(!must_be.get(literal));
    return might_be.remove(literal);
  }

  inline void CoreBased::pure_mark_backbone(Lit literal){
    if (!sign(literal)){
      backbones.insert(var(literal));
    } else{
      backbones.insert(-var(literal));
    }
    assert(might_be.get(literal));    
    might_be.remove(literal);
    might_be.remove(~literal);
    must_be.add(literal);
    sat_solver.addClause(literal);
  }

  bool CoreBased::run_sat_solver(const vec<Lit>& assumptions) {
    const auto t0 = read_cpu_time();
    const bool retv = sat_solver.solve(assumptions);
    // cout<<read_cpu_time() - t0<<endl;
    const auto t1 = read_cpu_time();
    solver_time+=(t1-t0);
    ++solver_calls;
    if (retv){
      models.insert(string_model(sat_solver.model));
    }
    return retv;
  }

  lbool CoreBased::run_sat_solver_limited(const vec<Lit>& assumptions) {
    size_t time_threshold = base_time;
    const auto t0 = read_cpu_time();
    float conf_cnt = sat_solver.conflicts;
    if(conf_cnt > 0 && sat_solver.conflicts < base_conflicts * 100){
      sat_solver.setConfBudget(previous_conflicts*conflict_ratio);
    } else{
      sat_solver.budgetOff();
    }
    if(!budget){
      sat_solver.budgetOff();
    }
    const lbool retv = sat_solver.solveLimited(assumptions);
    // cout<<read_cpu_time() - t0<<endl;
    previous_solver_time = read_cpu_time() - t0;
    if(conflict_ratio > 0.000000001){
      if(read_cpu_time() - t0 > time_threshold){
        conflict_ratio = conflict_ratio * 0.001;
      } else {
        conflict_ratio = conflict_ratio * 0.99;
      }
    }
    const auto t1 = read_cpu_time();
    solver_time+=(t1-t0);
    ++solver_calls;
    return retv;
  }

  bool CoreBased::run_sat_solver() {
    const vec<Lit> assumptions;
    return run_sat_solver(assumptions);
  }

}

